import React from 'react';
import Header from './components/Header';
import Counter from './components/Counter';
import Form from "./components/Form";
import Test from './components/UseEffect';

class App extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <Counter />
                <Test />
                {/* <Form /> */}
            </div>

        )
    }
};

export default App;